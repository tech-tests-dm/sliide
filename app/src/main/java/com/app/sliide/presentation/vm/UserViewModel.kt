package com.app.sliide.presentation.vm

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.sliide.data.remote.dto.UserDto
import com.app.sliide.domain.model.User
import com.app.sliide.domain.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject


@HiltViewModel
class UserViewModel @Inject constructor(
    private val repository: UserRepository
) : ViewModel() {

    val TAG = "UserDmesVmCoroutines"

    var _users = mutableStateOf<List<User>>(listOf())
    var users: State<List<User>> = _users

    val isLoading = MutableLiveData<Boolean>(false)
    val isError = MutableLiveData<Boolean>(false)


    val errorHandler = CoroutineExceptionHandler { _, throwable ->
        if (throwable is Exception) {
            isError.value = true
        }
    }

    init {
        Log.d("gg", "dm:: UserDm ViewModes coroutines")
    }


    fun loadUsers() {
        isLoading.value = true
        Log.d("gg", "dm:: isLoading 1 = ${isLoading.value}")
        viewModelScope.launch(errorHandler) {

            val resp: Response<List<UserDto>> = repository.getUsers()

            if (resp.isSuccessful) {
                Log.d("gg", "dm:: success code = ${resp.code()}")

                val listResult: List<UserDto>? = resp.body()
                _users.value = listResult?.map { it.toUser() } ?: listOf()

                Log.d("gg", "dm:: size ${listResult?.size}")

            } else {
                isLoading.value = false
                isError.value = true
                Log.d("gg", "dm:: Error message = ${resp.raw()}")
                Log.d("gg", "dm:: Error message = ${resp.errorBody()}")
            }
            isLoading.value = false
            Log.d("gg", "dm:: isLoading 2 = ${isLoading.value}")

        }
    }


    fun saveUser(user: User, success: () -> Unit, notSuccess: (String) -> Unit) {
        isLoading.value = true
        viewModelScope.launch {

            Log.d(TAG, "dm:: Saving user : ${user}")
            try {
                val resp = repository.saveUser(user)
                if (resp.isSuccessful) {
                    success()
                } else {
                    notSuccess("code = ${resp.code()}")
                    Log.d(TAG, "dm:: Saving error : ${resp.message()}")
                }
                isLoading.value = false
            } catch (e: Exception) {
                Log.d(TAG, "dm:: Saving error : ${e.message}")
                notSuccess("Something with server!")
                isLoading.value = false
            }
        }
    }


    fun deleteDish(id: Int, showMessage: (code: Int) -> Unit) =
        viewModelScope.launch {

            Log.d(TAG, "dm:: dd dish id - $id ")

            try {
                val resp = repository.deleteUser(id)

                if (resp.isSuccessful) {
                    val code: Int = resp.code().also {
                        Log.d(TAG, "dm:: User is deleted! (Code: $it)")
                    }
                    showMessage.invoke(code)
                } else {
                    Log.d(TAG, "dm:: dd  is NOT Successful = ${resp.code()}")
                    Log.d(TAG, "dm:: dd  is NOT Successful = ${resp.message()}")
                    showMessage.invoke(resp.code())
                }

            } catch (e: Exception) {
                Log.d(TAG, "dm:: dd listDishesForCurrentUser Error: ${e.message}")
            }

        }
}