package com.app.sliide.presentation.screens

import android.util.Log
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.app.sliide.data.remote.dto.UserDto
import com.app.sliide.domain.model.User
import com.app.sliide.presentation.components.ErrorUI
import com.app.sliide.presentation.components.LoadingUI
import com.app.sliide.presentation.vm.UserViewModel
import kotlinx.coroutines.launch

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun UsersScreen(
    vm: UserViewModel = hiltViewModel()
) {

    val users: List<User> by vm.users
    val isLoading: Boolean by vm.isLoading.observeAsState(false)
    val isError: Boolean by vm.isError.observeAsState(false)

    vm.loadUsers()

    val bottomSheetScaffoldState = rememberBottomSheetScaffoldState(
        bottomSheetState = BottomSheetState(BottomSheetValue.Collapsed)
    )
    val coroutineScope = rememberCoroutineScope()


    var fullNameState by remember { mutableStateOf("") }
    var emailState by remember { mutableStateOf("") }


    var dialogStateSuccess by remember { mutableStateOf(false) }
    if (dialogStateSuccess) {
        ShowDialogSuccess() {
            dialogStateSuccess = false
            vm.loadUsers()
        }
    }

    var dialogStateError by remember { mutableStateOf(false) }
    var textError by remember { mutableStateOf("false") }
    if (dialogStateError) {
        ShowDialogError(textError) { dialogStateError = false }
    }

    var dialogStateLong by remember { mutableStateOf(false) }
    var userIdDelete: Int by remember { mutableStateOf(0) }
    if (dialogStateLong) {
        ShowDialogLongClick(
            onDismiss = { dialogStateLong = false },
            onClickYes = {
                dialogStateLong = false
                vm.deleteDish(userIdDelete) {
                    vm.loadUsers()
                }
            }
        )
    }


    BottomSheetScaffold(
        sheetShape = RoundedCornerShape(topEnd = 30.dp, topStart = 30.dp),

        topBar = {
            TopAppBar(
                title = { Text(text = "Users") },
                actions = {
                    IconButton(
                        onClick = {
                            Log.d("ggg", "dm:: click bottom Sheet ")
                            coroutineScope.launch {
                                if (bottomSheetScaffoldState.bottomSheetState.isCollapsed) {
                                    bottomSheetScaffoldState.bottomSheetState.expand()
                                } else {
                                    bottomSheetScaffoldState.bottomSheetState.collapse()
                                }
                            }
                        },
                        content = { Icon(Icons.Filled.AddCircle, "add dish") }
                    )
                })
        },
        scaffoldState = bottomSheetScaffoldState,
        sheetContent = {

            Column(
                Modifier
                    .padding(16.dp, 16.dp, 16.dp, 54.dp),
            ) {

                Spacer(modifier = Modifier.height(8.dp))

                Text(
                    "Add user",
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth()
                )

                Spacer(modifier = Modifier.height(16.dp))


                TextField(
                    value = fullNameState,
                    onValueChange = { fullNameState = it },
                    placeholder = { Text("Enter name of user.") })

                TextField(
                    value = emailState,
                    onValueChange = { emailState = it },
                    placeholder = { Text("Enter email of user.") }
                )

                Row(Modifier.padding(0.dp, 16.dp, 0.dp, 16.dp)) {
                    Button(
                        onClick = {
                            fullNameState = ""
                            emailState = ""
                        },
                        modifier = Modifier
                            .weight(1f)
                            .padding(4.dp),
                        shape = RoundedCornerShape(20),
                        content = {
                            Text("Clear")
                        }
                    )

                    Button(
                        onClick = {
                            vm.saveUser(
                                user = User(null, fullNameState, emailState, null, null),
                                success = {
                                    coroutineScope.launch {
                                        bottomSheetScaffoldState.bottomSheetState.collapse()
                                    }
                                    fullNameState = ""
                                    emailState = ""
                                    dialogStateSuccess = true

                                },
                                notSuccess = { errorText ->
                                    textError = errorText
                                    dialogStateError = true
                                }
                            )
                        },
                        modifier = Modifier
                            .weight(1f)
                            .padding(4.dp),
                        shape = RoundedCornerShape(20),
                        content = { Text("Save") }
                    )
                }

            }
        },
        content = {
            when {
                isLoading -> LoadingUI()
                isError -> ErrorUI()
                else -> {
                    LazyColumn() {
                        items(users) {
                            UserItem(it) {
                                userIdDelete = it
                                dialogStateLong = true
                            }
                        }
                    }
                }
            }
        }
    )
}

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Preview
@Composable
fun MyDishesScreen_Preview() {
    UsersScreen()
}


@ExperimentalFoundationApi
@Composable
fun UserItem(user: User, delete: (Int) -> Unit) =
    Row(
        modifier = Modifier
            .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
            .fillMaxWidth()
            .combinedClickable(
                onClick = { },
                onLongClick = {
                    Log.d("ff", "dm:: Long click!!")
                    user.id?.let { delete.invoke(it) }
                }
            ),
        content = {
            Column() {

                Spacer(modifier = Modifier.height(2.dp))

                Row(verticalAlignment = Alignment.CenterVertically) {
                    Text(user.name)
                }

                Spacer(modifier = Modifier.height(2.dp))

                Text(user.email)
            }
        })


@Composable
fun ShowDialogSuccess(onDismiss: () -> Unit) {

    AlertDialog(
        backgroundColor = MaterialTheme.colors.surface,
        text = { Text("User is saved") },
        buttons = {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 16.dp, end = 16.dp),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.Top,
                content = {

                    Spacer(modifier = Modifier.width(16.dp))
                    Button(onClick = onDismiss) {
                        Text(text = "Ok")
                    }

                })
        },
        onDismissRequest = onDismiss
    )

}


@Composable
fun ShowDialogError(errorText: String, onDismiss: () -> Unit) {

    AlertDialog(
        backgroundColor = MaterialTheme.colors.surface,
        text = { Text("User is not saved. try again, $errorText") },
        buttons = {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 16.dp, end = 16.dp),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.Top,
                content = {
                    Button(onClick = onDismiss) {
                        Text(text = "Ok")
                    }

                })
        },
        onDismissRequest = onDismiss
    )

}


@Composable
fun ShowDialogLongClick(onDismiss: () -> Unit, onClickYes: () -> Unit) {

    AlertDialog(
        backgroundColor = MaterialTheme.colors.surface,
        text = { Text("Are you sure you want to delete this user?") },
        buttons = {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 16.dp, end = 16.dp),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.Top,
                content = {
                    TextButton(onClick = onDismiss) {
                        Text(text = "No")
                    }
                    Spacer(modifier = Modifier.width(16.dp))
                    Button(onClick = onClickYes) {
                        Text(text = "Yes")
                    }
                })
        },
        onDismissRequest = onDismiss
    )

}