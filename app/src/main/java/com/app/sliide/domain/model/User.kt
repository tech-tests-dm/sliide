package com.app.sliide.domain.model

import com.app.sliide.data.remote.dto.UserDto


data class User(
    val id: Int?,
    val name: String,
    val email: String,
    val gender: String?,
    val status: String?
){

    fun toUserDto() =
        UserDto(
            id = id,
            name = name,
            email = email,
//            gender = gender ,
//            status = status
        )
}