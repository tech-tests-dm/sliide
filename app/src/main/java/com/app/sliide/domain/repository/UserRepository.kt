package com.app.sliide.domain.repository

import com.app.sliide.data.remote.dto.UserDto
import com.app.sliide.domain.model.User
import retrofit2.Response

interface UserRepository {

    suspend fun getUsers(): Response<List<UserDto>>
    suspend fun saveUser(request: User): Response<Any>
    suspend fun deleteUser(id: Int): Response<Void>
}