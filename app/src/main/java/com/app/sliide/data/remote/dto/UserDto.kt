package com.app.sliide.data.remote.dto

import com.app.sliide.domain.model.User
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class UserDto(

    val id: Int? = null,
    val name: String,
    val email: String,
    val gender: String = "male",
    val status: String = "active"

) {

    fun toUser() =
        User(
            id = id,
            name = name,
            email = email,
            gender = gender,
            status = status
        )
}