package com.app.sliide.data

import com.app.sliide.data.remote.dto.UserDto
import retrofit2.Response
import retrofit2.http.*


interface RetrofitApiService {

    @GET("public/v2/users?page=1&access-token=01cdb2c86d8d98b8f3335515c1001c6db59d3a1cc9840474731277275a3546c9")
    suspend fun getUsers(  ): Response<List<UserDto>>

    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST("public/v2/users?access-token=01cdb2c86d8d98b8f3335515c1001c6db59d3a1cc9840474731277275a3546c9")
    suspend fun saveUser(@Body userDto: UserDto): Response<Any>

    @Headers("Accept:application/json", "Content-Type:application/json")
    @DELETE("/public/v2/users/{id}?access-token=01cdb2c86d8d98b8f3335515c1001c6db59d3a1cc9840474731277275a3546c9")
    suspend fun deleteUser(@Path("id") id: Int): Response<Void>


    companion object {
        const val BASE_URL = "https://gorest.co.in/"
    }
}


