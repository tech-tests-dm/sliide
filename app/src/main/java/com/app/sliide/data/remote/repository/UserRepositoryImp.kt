package com.app.sliide.data.remote.repository

import com.app.sliide.data.RetrofitApiService
import com.app.sliide.data.remote.dto.UserDto
import com.app.sliide.domain.model.User
import com.app.sliide.domain.repository.UserRepository
import retrofit2.Response

class UserRepositoryImp(
    private val retrofitApiService: RetrofitApiService
) : UserRepository {

    override suspend fun getUsers(): Response<List<UserDto>> = retrofitApiService.getUsers()

    override suspend fun saveUser(user: User) = retrofitApiService.saveUser(user.toUserDto())

    override suspend fun deleteUser(id: Int): Response<Void> = retrofitApiService.deleteUser(id)
}