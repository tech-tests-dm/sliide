package com.app.sliide

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
open class ProKitApp : Application() {


}