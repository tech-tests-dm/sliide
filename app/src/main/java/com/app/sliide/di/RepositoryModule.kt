package com.app.sliide.di

import com.app.sliide.data.RetrofitApiService
import com.app.sliide.data.remote.repository.UserRepositoryImp
import com.app.sliide.domain.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideUserRepository(retrofitService: RetrofitApiService): UserRepository =
        UserRepositoryImp(retrofitService)

}

